package ictgradschool.industry.lab16.ex02.gui;

import ictgradschool.industry.lab16.ex02.model.Course;
import ictgradschool.industry.lab16.ex02.model.CourseListener;

import java.awt.*;

public class StatisticsPanelAdapter extends StatisticsPanel implements CourseListener{
	/**
	 * Creates a StatisticsPanel object.
	 *
	 * @param course
	 */
	public StatisticsPanelAdapter(Course course) {
		super(course);
	}

	@Override
	public void courseHasChanged(Course course) {
		repaint();

	}

	/**********************************************************************
	 * YOUR CODE HERE
	 */
	
}
