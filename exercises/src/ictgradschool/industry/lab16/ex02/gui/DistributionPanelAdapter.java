package ictgradschool.industry.lab16.ex02.gui;

import ictgradschool.industry.lab16.ex02.model.Course;
import ictgradschool.industry.lab16.ex02.model.CourseListener;

import java.awt.*;

public class DistributionPanelAdapter extends DistributionPanel implements CourseListener{
	/**
	 * Creates a DistributionPanel object.
	 *
	 * @param course
	 *
	 */


	public DistributionPanelAdapter(Course course) {
		super(course);

	}

	@Override
	public void courseHasChanged(Course course) {
		repaint();
	}


	/**
	 * Creates a DistributionPanel object.



	/**********************************************************************
	 * YOUR CODE HERE
	 */
}
