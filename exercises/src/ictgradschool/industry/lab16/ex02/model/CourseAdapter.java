package ictgradschool.industry.lab16.ex02.model;


import javax.swing.table.AbstractTableModel;

public class CourseAdapter extends AbstractTableModel implements CourseListener{

	Course course;

	public CourseAdapter(Course course){
		this.course = course;
	}

	@Override

	public void courseHasChanged(Course course) {

	}

	@Override
	public int getRowCount() {
		return course.size();
	}

	@Override
	public int getColumnCount() {
		return 7;
	}

	@Override
	public String getColumnName(int column) {
		final String[] names = new String[] {"Student ID", "Surname", "Firstname", "Exam", "Test", "Assignment", "Overall"};
		return names[column];
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		StudentResult studentResult = course.getResultAt(rowIndex);
		String[] results = studentResult.toString().split(" ");
		switch (columnIndex) {
			case 0:
				return studentResult._studentID;
			case 1:
				return studentResult._studentSurname;
			case 2:
				return studentResult._studentForename;
			case 3:
				return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Exam);
			case 4:
				return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Test);
			case 5:
				return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Assignment);
			case 6:
				return studentResult.getAssessmentElement(StudentResult.AssessmentElement.Overall);
			default:return "Nothing";
		}
	}

	/**********************************************************************
	 * YOUR CODE HERE
	 */
}
